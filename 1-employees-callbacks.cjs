/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

const fs = require('fs');

function main(dataFile) {

    fs.readFile(dataFile, (error, data) => {

        if (error) {

            console.error(error);

        } else {

            let employeeObject = JSON.parse(data.toString());
            let employeesArray = employeeObject['employees'];

            // 1. Retrieve data for ids : [2, 13, 23].

            let retriveDataForParticularIds = employeesArray
                .reduce((filteredEmployee, employee, employeeIndex, allEmployee) => {

                    if (employee['id'] == 2 || employee['id'] == 13 || employee['id'] == 23) {

                        filteredEmployee.push(employee);

                    }

                    return filteredEmployee;

                }, []);

            fs.writeFile('retriveDataForIds.json', JSON.stringify(retriveDataForParticularIds), (error) => {

                if (error) {

                    console.error(error);

                } else {

                    console.log('File created for ids 2, 13, 23');

                }

            });

            console.log(retriveDataForParticularIds);

            // 2. Group data based on companies.
            // { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

            let companiesGroup = { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] };

            let companiesName = Object.keys(companiesGroup);

            let companyGroups = employeesArray.reduce((compnyGroup, employee, employeeIndex, allEmployee) => {

                if (companiesGroup[employee['company']]) {

                    companiesGroup[employee['company']].push(employee);

                }

                return companiesGroup;

            }, { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] });

            console.log(companiesGroup);

            fs.writeFile('companyGroups.json', JSON.stringify(companiesGroup), (error) => {

                if (error) {

                    console.error(error);

                } else {

                    console.log('companyGroups created');

                }

            });

            // 3. Get all data for company Powerpuff Brigade

            let powerpuffDetails = employeesArray.reduce((powerpuffCompany, employee, employeeIndex, allEmployee) => {

                if (employee["company"] === "Powerpuff Brigade") {

                    powerpuffCompany["Powerpuff Brigade"].push(employee);

                }

                return powerpuffCompany;

            }, { "Powerpuff Brigade": [] });

            console.log(powerpuffDetails);

            fs.writeFile('powerpuffDetails.json', JSON.stringify(powerpuffDetails), (error) => {

                if (error) {

                    console.error(error);

                } else {

                    console.log('Collected all details for the powerpuff brigade');

                }

            });

            // 4. Remove entry with id 2.

            let employeesArrayWithoutId2 = employeesArray.reduce((employeeArray, employee, employeeIndex, allEmpoylee) => {

                if (employee['id'] == 2) {

                    employeeArray.splice(employeeIndex, 1);

                }

                return employeeArray;

            }, [...employeesArray]);

            console.log(employeesArrayWithoutId2);

            fs.writeFile('deletedEntryId2.json', JSON.stringify(employeesArrayWithoutId2), (error) => {

                if (error) {

                    console.error(error);

                } else {

                    console.log('file created to store the employees without entry 2');

                }

            });

            // 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric

            let sortedEmployeesByCompany = employeesArray.sort((firstEmployee, secondEmployee) => {

                if (firstEmployee['company'] > secondEmployee['company']) {

                    return 1;

                } else if (firstEmployee['company'] < secondEmployee['company']) {

                    return -1;

                } else {

                    if (firstEmployee['id'] > secondEmployee['id']) {

                        return 1;

                    } else if (firstEmployee['id'] < secondEmployee['id']) {

                        return -1;

                    } else {

                        return 0;

                    }

                }
            })

            console.log(sortedEmployeesByCompany);

            fs.writeFile('sortedEmployeeData.json', JSON.stringify(sortedEmployeesByCompany), (error) => {

                if (error) {

                    console.error(error);

                } else {

                    console.log('SortedEmployee data file created');

                }

            });

            // 6. Swap position of companies with id 93 and id 92.

            // start Here

            // 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

            let addingBirthday = employeesArray.map((employee, employeeIndex, allEmployee) => {

                if (employee.id % 2 == 0) {

                    employee['birthData'] = new Date().toString().split(' ').slice(0, 4).join(' ');

                    return employee;

                } else {

                    return employee;

                }

            });

            console.log(addingBirthday);

            fs.writeFile('DOBaddedToEmployee.json', JSON.stringify(addingBirthday), (error) => {

                if (error) {

                    console.error(error);

                } else {

                    console.log('file created to cantains the bithday to the employee haveing even ids');

                }

            });

        }

    });

}

main('./data.json');