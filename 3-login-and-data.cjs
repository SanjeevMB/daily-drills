/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

*/

// import { resolve } from 'path';

// function login(user, val) {
//     if (val % 2 === 0) {
//         return Promise.resolve(user);
//     } else {
//         return Promise.reject(new Error("User not found"));
//     }
// }

// function getData() {
//     return Promise.resolve([
//         {
//             id: 1,
//             name: "Test",
//         },
//         {
//             id: 2,
//             name: "Test 2",
//         }
//     ]);
// }

// function logData(user, activity) {
//     // use promises and fs to save activity in some file
// }

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.    
*/

const fs = require('fs');
const path = require('path');

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

function fileCreater(fileName, content) {

    return new Promise((resolve, rejects) => {

        fs.writeFile(fileName, content, (error) => {

            if (error) {

                rejects(error);

            } else {

                resolve(fileName);

            }

        });

    });

}

function fileDeleter(fileName) {

    return new Promise((resolve, rejects) => {

        fs.unlink(`${fileName}`, (error) => {

            if (error) {

                rejects(error);

            } else {

                resolve(`${fileName} deleted`);

            }

        });

    });

}

function fileReader(fileName) {

    return new Promise((resolve, rejects) => {

        fs.readFile(fileName, (error, data) => {

            if (error) {

                rejects(error);

            } else {

                resolve(data);

            }

        });

    });

}

function createFileAndDeleteSimultaneously() {

    fileCreater('firstFile.txt', '').then((data) => {

        console.log(`${data} created`);

    }).catch((data) => {

        console.error(data);

    });

    fileCreater('secondFile.txt', '').then((data) => {

        console.log(`${data} created`);

    }).catch((data) => {

        console.log(data);

    });

    setTimeout(() => {

        (fileDeleter('firstFile.txt')).then((data) => {

            console.log(data);

        }).catch((data) => {

            console.error(data);

        });

        (fileDeleter('secondFile.txt')).then((data) => {

            console.log(data);

        }).catch((data) => {

            console.error(data);

        });

    }, 2000);

}

createFileAndDeleteSimultaneously();

// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining

function copyingLipsumDataToNewFileAndDeletingLipsum() {

    fileCreater('lipsum.txt', 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quas laborum cumque tenetur quae magni aspernatur repudiandae unde ipsum possimus assumenda nostrum, nihil laudantium facere dolorum. Soluta esse commodi ullam, blanditiis itaque architecto ut. Repellendus, quos! Facilis, veritatis minima molestiae assumenda ipsam laudantium quidem nesciunt sed repudiandae maiores corporis itaque doloremque.')
        .then((data) => {

            return fileReader(data);

        }).then((data) => {

            fileCreater('copyLipsumData.txt', data);

            return new Promise((resolve, rejects) => {

                resolve('lipsum.txt');

            });

        }).then((data) => {

            return fileDeleter(data);

        }).then((data) => {

            console.log((data));

        }).catch((data) => {

            console.error(data);

        });

}

// copyingLipsumDataToNewFileAndDeletingLipsum();