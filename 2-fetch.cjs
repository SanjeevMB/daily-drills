
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended
*/

const { captureRejectionSymbol } = require('events');
let path = require('path');

function fetchData(url) {

    return new Promise((resolve, rejects) => {

        fetch(url).then((data) => {

            resolve(data.json());

        }).catch((error) => {

            rejects(error);

        });

    });

}

// 1. Fetch all the users

function fetchUsers() {

    let userData = fetchData('https://jsonplaceholder.typicode.com/users');

    userData.then((usersData) => {

        console.log(usersData);

    }).catch((error) => {

        console.error(error);

    });

}

fetchUsers();

// 2. Fetch all the todos

function fetchTodos() {

    let todoData = fetchData('https://jsonplaceholder.typicode.com/todos')

    todoData.then((todoData) => {

        console.log(todoData);

        return todoData;

    }).catch((error) => {

        console.error(error);

    });

}

fetchTodos();

// 3. Use the promise chain and fetch the users first and then the todos.

function promiseChaintoFetchData() {

    let userData = fetchData('https://jsonplaceholder.typicode.com/users');

    userData.then((usersData) => {

        console.log(usersData);

    }).catch((error) => {

        console.error(error);

    }).then(() => {

        let todoData = fetchData('https://jsonplaceholder.typicode.com/todos')

        todoData.then((todoData) => {

            console.log(todoData);

        }).catch((error) => {

            console.error(error);

        });

    }).catch((error) => {

        console.error(error);

    });

}

promiseChaintoFetchData();

// 4. Use the promise chain and fetch the users first and then all the details for each user.

function usersDetailByPromiseChain() {

    let userData = fetchData('https://jsonplaceholder.typicode.com/users');

    userData.then((usersData) => {

        return usersData;

    }).catch((error) => {

        console.error(error);

    }).then((userData) => {

        let usersDetails = userData.map((user, userIndex, array) => {

            return user;

        });

        console.log(usersDetails);

    });

}

usersDetailByPromiseChain();

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

function userDetailFromFirstTodo() {

    let todoData = fetchData('https://jsonplaceholder.typicode.com/todos')

    todoData.then((todoData) => {

        return todoData[0];

    }).catch((error) => {

        console.error(error);

    }).then((firstTodo) => {

        let associatedId = firstTodo.id;

        let userData = fetchData('https://jsonplaceholder.typicode.com/users');

        userData.then((usersData) => {

            let associatedUserWithTodo = usersData.filter((user, userIndex, allUsers) => {

                return user.id === associatedId;

            });

            console.log(associatedUserWithTodo);

        }).catch((error) => {

            console.error(error);

        });

    });

}

userDetailFromFirstTodo();